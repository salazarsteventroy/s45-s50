// mock database
const courseData = [
	{
		id : "wdc001",
		name : "PHP - Laravel",
		description: "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...",
		price: 45000,
		onOffer: true
	},
	{
			id : "wdc002",
		name : "Python - Django",
		description: "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...",
		price: 50000,
		onOffer: true
	},
	{
			id : "wdc003",
		name : "Java - Springboot",
		description: "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...",
		price: 55000,
		onOffer: true
	},
	{
			id : "wdc004",
		name : "HTML Introduction",
		description: "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...",
		price: 60000,
		onOffer: true
	}
];

export default courseData;
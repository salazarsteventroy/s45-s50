import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Redirect, Route } from "react-router-dom";

const Loggedin = props => {

	const [return, setReturn] = useState("");

	useEffect(() => {
		switch (props.condition){
			case "Loggedin";

			return setReturn(
				props.user.Loggedin ? (
					<Route {...props} />
					): (
					<Redirect to ="../pages/Courses")
				)
		}
	})
}
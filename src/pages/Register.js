import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';



export default function Register(){

	const {user} = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');	
	const [Fname, setFname ] = useState ('');
	const [Lname, setLname ] = useState ('');
	const [Age, setAge] = useState ('');
	const [mobileNo, setMobileNo] = useState ('');

	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Check if values are successfully binded
	console.log(email);
	console.log(password1);
	console.log(password2);	

	// Function to simulate user registration
	function registerUser(e) {
		// Prevents page redirection via form submission
		e.preventDefault();

		// Clear input fields
		setEmail('');
		setPassword1('');
		setPassword2('');
		setFname('');
		setLname('');
		setAge('');
		setMobileNo('');
		alert('Thank you for registering!')
	}

/*const { check } = require('express-validator')
const repo = require('./repository')
const { validateEmail } = require('./validator')  

module.exports = {
  
    validateEmail: check('email')
  
        .custom(async (email) => {
            const existingUser = 
                await repo.getOneBy({ email })
                  
            if (existingUser) {
                throw new Error('Email already in use')
            }
        })
}*/


	Router.post('http://localhost:4000/api/users/Register',(req,res)=> {
		const {email} = req.body.email;
		user.findOne({email:email}).then(user=>{
			if(user){
				  Swal.fire({
                    title: "Email Exists",
                    icon: "error",
                    text: "This email already exists"
                })
				 }
            else{
                Swal.fire({
                    title: "Success",
                    icon: "success",
                    text: "Account Successfully Created."
                })
			}
		})
	})

	useEffect(() => {
		// Validation to enable the submit buttion when all fields are populated and both passwords match
		if((email !== '' && password1 !== '' && password2 !== '' && Fname !== '' && Lname !== '' && Age !== '' && mobileNo !== '') && (password1 === password2)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [Fname, Lname, Age, mobileNo, email, password1, password2])

	return (
		(user.id !== null) ?
		    <Redirect to="/Login" />
		:
		<Container>
			<h1>Register</h1>

			<Form className="mt-3" onSubmit={(e) => registerUser(e)}>

			{/*First Name*/}
			  <Form.Group className="mb-3" controlId="Fname">

			    <Form.Label>First Name</Form.Label>
			    <Form.Control 
			    	type="text" 
			    	placeholder="Enter First Name" 
			    	value = {Fname}
			    	onChange = { e => setFname(e.target.value)}
			    	required 
			    />
			  </Form.Group>
			
			   <Form.Group className="mb-3" controlId="userLname">
			  	
			    <Form.Label>Last Name</Form.Label>
			    <Form.Control 
			    	type="text" 
			    	placeholder="Enter Last Name" 
			    	value = {Lname}
			    	onChange = { e => setLname(e.target.value)}
			    	required 
			    />
			  </Form.Group>
			{/*Last Name*/}
			   <Form.Group className="mb-3" controlId="userAge">
			  	
			    <Form.Label>Age</Form.Label>
			    <Form.Control 
			    	type="number" 
			    	placeholder="Enter Age" 
			    	value = {Age}
			    	onChange = { e => setAge(e.target.value)}
			    	required 
			    />
			  </Form.Group>
			{/*Age*/}
			   <Form.Group className="mb-3" controlId="mobileNo">
			  	
			    <Form.Label>Mobile Number</Form.Label>
			    <Form.Control 
			    	type="tel" 
			    	placeholder="Enter Mobile No." 
			    	value = {mobileNo}
			    	maxlength = "11"
			    	onChange = { e => setMobileNo(e.target.value)}
			    	required 
			    />
			  </Form.Group>

			{/*Mobile No.*/}

			 <Form.Group className="mb-3" controlId="userEmail">
			  	
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email" 
			    	value = {email}
			    	onChange = { e => setEmail(e.target.value)}
			    	required 
			    />
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="password1">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Password" 
			    	value={password1}
			    	onChange = { e => setPassword1(e.target.value)}
			    	required 
			    />
			  </Form.Group>
			  
			  <Form.Group className="mb-3" controlId="password2">
			    <Form.Label>Verify Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Verify Password" 
			    	value={password2}
			    	onChange = { e => setPassword2(e.target.value)}
			    	required 
			    />
			  </Form.Group>
			{/* Conditionally render submit button based on isActive state */}
			  { isActive ? 
			  		<Button variant="primary" type="submit" id="submitBtn">
			  		  Submit
			  		</Button>
			  	:
				  	<Button variant="primary" type="submit" id="submitBtn" disabled>
				  	  Submit
				  	</Button>
			  }
			  
			</Form>
		</Container>
	)
}

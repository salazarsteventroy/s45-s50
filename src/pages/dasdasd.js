import emailCheck from "email-check";
//other imports

router.post("/register", (req, res) => {
    var name = req.body.name;
    var email = req.body.email;
    var password = req.body.password;
    var confirmedPassword = req.body.confirmedPassword;

    // your validation for another fields
    emailCheck(email)
        .then(() => {
            User.create(req.body)
                .then(() => {
                    res.send(req.body);
                })
                .catch((error) =>
                    res.json({serverErrorDublicateEmail: "The email address is already subscribed. Please try to use another one or simply Log in"});
                });
        })
        .catch(() => {
            res.json({serverErrorEmailExistence: "The email address doesn't exist. Please try the valid one"});
        });
});